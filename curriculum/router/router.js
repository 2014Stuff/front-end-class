var express = require('express');
var router = express.Router();
var fs = require('fs');

router.get('/*', function (req, res) {
  fs.readFile('./public/index.html', {
      encoding: 'utf8'
    },
    function (err, data) {
      if (err) {
        res.status(500).send('something went wrong');
      } else {
        res.send(data);
      }
    });
});

module.exports = router;