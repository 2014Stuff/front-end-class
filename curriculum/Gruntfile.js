module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    'bower-install-simple': {
      options: {
        color: false
      }
    },
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('./public'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              )
            ];
          }
        }
      }
    },
    jshint: {
      options: {
        ignores: ['public/components/**/*.js']
      },
      files: {
        src: ['Gruntfile.js', 'app.js', 'router/*.js', 'public/**/*.js', 'test/**/*.js']
      }
    },
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['./public/scripts/{,*/}*.js'],
        tasks: ['jshint'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      // jsTest: {
      //   files: ['test/spec/{,*/}*.js'],
      //   tasks: ['karma']
      // },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          './public/{,*/}*.html',
          './public/styles/{,*/}*.css'
        ]
      }
    },
    wiredep: {
      app: {
        src: ['./public/index.html'],
        ignorePath: /\.\.\//
      }
    }
  });

  grunt.loadNpmTasks("grunt-bower-install-simple");
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-wiredep');

  // Default task(s).
  grunt.registerTask('serve', ['bower-install-simple', 'wiredep', 'jshint', 'connect:livereload', 'watch']);

};