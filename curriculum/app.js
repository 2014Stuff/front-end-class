var express = require('express'),
  cors = require('cors'),
  body = require('body-parser'),
  ip = require('ip'),
  log4js = require('log4js'),
  router = require('./router/router.js'),
  // config = require('./config/config.json'),
  // db = require('./lib/db'),
  port = 3000,
  app = express(),
  log = log4js.getLogger();

// emitter = new events.EventEmitter(),
// port = config.port,
// mongoUrl = 'mongodb://' + config.mongo.address + '/chatter',
// server, manager;


// parse application/json
app.use(body.json());

//cors
// app.use(cors());

// logger
app.use(function (req, res, next) {
  log.info('%s %s', req.method, req.url);
  log.info('Body:', req.body);
  next();
});
//public
app.use(express.static(__dirname + '/public'));

//set routes
// route(app, emitter);

app.use('/', router);

app.listen(port);
log.info('Listening @', ip.address() + ':' + port);

// db.connect(mongoUrl, function () {
//   console.log('  info -- db.js || connected to db @:', mongoUrl);
// });