angular.module('curriculum.services', [])
  .service('contentManager', [
    '$http',
    function ($http) {
      var db = {
        backend: {
          languages: ["JavaScript", "PHP", ".NET"],
          JavaScript: [{
            language: "javascript",
            anchor: "starting_a_server",
            title: "Starting a server",
            notes: "Starting a server using Node.js and Express is a piece of cake. In three lines of code you can have it up and running. With a few more lines you can start hosting your own application/content.",
            content: "var express = require('express');\nvar app = express();\napp.get('/', function (req, res) {\n  res.send('Hello World');\n})\napp.listen(3000);"
          }]
        }
      };
      return {
        getLanguages: function (category, cb) {
          if (db[category] && db[category].languages) {
            cb(null, db[category].languages);
          } else {
            cb(new Error('not found'));
          }
        },
        getTopics: function (category, language, cb) {
          if (db[category] && db[category][language]) {
            cb(null, db[category][language]);
          } else {
            cb(new Error('not found'));
          }
        }
      };
    }
  ]);