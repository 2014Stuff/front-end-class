var app = angular.module('curriculum', [
  'ngRoute',
  'curriculum.controllers',
  'curriculum.services'
]);
app.config(function ($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partials/main.html'
    })
    .when('/frontend', {
      templateUrl: 'partials/content.html',
      controller: 'contentCtrl'
    })
    .when('/backend', {
      templateUrl: 'partials/content.html',
      controller: 'contentCtrl'
    })
    .when('/database', {
      templateUrl: 'partials/content.html',
      controller: 'contentCtrl'
    })
    .when('/misc', {
      templateUrl: 'partials/content.html',
      controller: 'contentCtrl'
    });

  // use the HTML5 History API
  $locationProvider.html5Mode(true);
});