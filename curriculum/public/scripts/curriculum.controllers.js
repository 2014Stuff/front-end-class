angular.module('curriculum.controllers', [])

.controller('mainCtrl', [
  '$scope',
  '$rootScope',
  '$location',
  'contentManager',
  function ($scope, $rootScope, $location, contentManager) {
    //query backend for available languages for specific learning module
    $scope.languages = ['JavaScript', '.NET', 'Ruby on Rails', 'Java'];
    $scope.language = $scope.languages[0];
    $scope.select = function (l) {
      $scope.language = l;
      $rootScope.$broadcast('langchang');
    };
    $scope.categories = [{
      title: "Frontend",
      anchor: "/frontend"
    }, {
      title: "Backend",
      anchor: "/backend"
    }, {
      title: "Database",
      anchor: "/database"
    }, {
      title: "Misc",
      anchor: "/misc"
    }];
    $scope.view = function (page, $e) {
      $location.url(page);
      var nav = angular.element($e.target);
      angular.forEach(angular.element('.nav-item'), function (e) {
        angular.element(e).removeClass('active');
      });
      nav.addClass('active');
    };
    $scope.category = '';
  }
])

.controller('contentCtrl', [
  '$scope',
  '$location',
  'contentManager',
  function ($scope, $location, contentManager) {
    $scope.topics = [];

    $scope.category = $location.path().substring(1);
    $scope.icon = '';

    $scope.$on('langchang', function () {
      getTopics();
    });

    contentManager.getLanguages($scope.category, function (err, languages) {
      $scope.$parent.languages = languages;
      var found = false;
      for (var l in languages) {
        if ($scope.$parent.language === $scope.$parent.languages[l]) {
          found = true;
        }
      }
      if (!found && languages && languages[0]) {
        $scope.$parent.language = languages[0];
      }
      getTopics();
    });

    function getTopics() {
      contentManager.getTopics($scope.category, $scope.$parent.language, function (err, topics) {
        $scope.topics = topics;
        setTimeout(function () {
          $scope.$apply(function () {
            Prism.highlightAll();
          });
        }, 5);
      });
    }

  }
]);