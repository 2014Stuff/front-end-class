var user = require('../lib/user.js'),
  chat = require('../lib/chat.js');

module.exports = function route(app, emitter) {
  //Chats
  app.post('/chats', function (req, res) {
    var owner = req.body.owner,
      msg = req.body;
    console.log('  info - routes.js || #route: announcing chat',
      'from', owner, ':', msg);
    emitter.emit('chat', owner, msg);
    chat.createNewChat(req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(500).send('createNewChat failed');
      } else {
        res.json(data);
      }
    });
  });

  //Users
  //POST
  app.post('/users', function (req, res) {
    user.createNewUser(req.body, function (err, data) {
      if (err) {
        console.log(err);
        res.status(500).send('createNewUser failed');
      } else {
        res.json(data);
      }
    });
  });
  //GET
  app.get('/users', function (req, res) {
    user.getAllUsers(function (err, users) {
      if (err) {
        console.log(err);
        res.status(500).send('getAllUsers failed');
      } else {
        // console.log('  debug -- routes.js # getAllUsers returned:', users);
        res.json(users);
      }
    });
  });
  app.get('/users/:id', function (req, res) {
    user.getUser(req.params.id, function (err, user) {
      if (err) {
        console.log(err);
        res.status(500).send('getUser failed');
      } else if (!user) {
        res.status(404).send('Id: ' + req.params.id + ' not found');
      } else {
        res.json(user);
      }
    });
  });

  //Friends
  //PUT
  app.put('/users/:id/:friend', function (req, res) {
    user.addFriend(req.params.id, req.params.friend, function (err, update) {
      if (err) {
        console.log(err);
        res.status(500).send('addFriend failed');
      } else {
        res.json(update);
      }
    });
  });
  //DELETE
  app.delete('/users/:id/:friend', function (res, res) {
    user.removeFriend(req.params.id, req.params.friend, function (err, update) {
      if (err) {
        console.log(err);
        res.status(500).send('removeFriend failed');
      } else {
        res.json(update);
      }
    });
  });
};