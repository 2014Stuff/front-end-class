var ws = require('ws'),
  uuid = require('node-uuid'),
  util = require('util'),
  user = require('../lib/user.js'),
  WebSocketServer = ws.Server;

function SocketManager(server, e) {
  var self = this;
  this.connections = {};

  console.log('  info -- manager.js || #init setting up websockets on port:',
    server.address().port);
  this.wss = new WebSocketServer({
    server: server
  });
  e.on('chat', function (id, message) {
    console.log('  info -- manager.js || got chat event:', id, message);
    if (self.connections[id]) {
      //console.log('  info -- manager.js || announcing to friends:', self.connections[id].user.friends);
      self.notifyFriends(self.connections[id].user.friends, message);
    }
  });
  this.wss.on('connection', function (ws) {
    ws._socketId = uuid.v4();
    ws.on('close', function () {
      console.log('Connection closed, removing socket:', this._socketId);
      self.removeConnection(this._socketId);
    });
    ws.on('message', function (data, flags) {
      var _this = this;
      var data = JSON.parse(data);
      if (data.type) {
        if (data.type === 'register') {
          console.log('registering new connection:', data.id);
          //Get friends of id from db
          user.getUser(data.id, function (err, user) {
            if (err) {
              throw err;
            }
            self.connections[data.id] = {
              user: user,
              ws: _this
            };
            var reply = {
              message: 'registered successfully',
              status: 200
            };
            self.notifyUser(data.id, reply);
          });
        } else if (data.type === 'add') {
          self.connections[data.id].user.friends.push(data.friendId);
        } else if (data.type === 'remove') {
          var i = self.connections[data.id].user.friends.indexOf(data.friendId);
          self.connections[data.id].user.friends.splice(i, 1);
        }
      } else {
        var err = new Error('invalid message type')
        ws.send(JSON.stringify({
          error: err,
          message: err.message
        }));
      }
      //console.log(flags);
    });
  });
};

SocketManager.prototype.removeConnection = function (id) {
  for (var c in this.connections) {
    if (this.connections[c].ws._socketId === id) {
      delete this.connections[c];
    }
  }
};


//@params: array of friend ids, json notification
SocketManager.prototype.notifyFriends = function (friends, notification) {
  var self = this;
  friends.forEach(function (f) {
    self.notifyUser(f, notification);
  });
};

SocketManager.prototype.notifyUser = function (id, message) {
  var self = this;
  if (self.connections[id].user && self.connections[id].ws) {
    console.log('  info -- manager.js || #notifyUser: notifying', id, 'message', message);
    self.connections[id].ws.send(JSON.stringify(message));
  }
};

module.exports = SocketManager;