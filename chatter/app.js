var events = require('events'),
  http = require('http'),
  express = require('express'),
  body = require('body-parser'),
  cors = require('cors'),
  ip = require('ip'),
  Manager = require('./socketManager'),
  route = require('./routes'),
  config = require('./config/config.json'),
  db = require('./lib/db'),
  app = express(),
  emitter = new events.EventEmitter(),
  port = config.port,
  mongoUrl = 'mongodb://' + config.mongo.address + '/chatter',
  server, manager;
// parse application/x-www-form-urlencoded
app.use(body.urlencoded({
  extended: false
}));
// parse application/json
app.use(body.json());
// parse application/vnd.api+json as json
app.use(body.json({
  type: 'application/vnd.api+json'
}));
//cors
app.use(cors());
// logger
app.use(function (req, res, next) {
  console.log('%s %s', req.method, req.url);
  console.log('Body:', req.body);
  next();
});
//public
app.use(express.static(__dirname + '/public'));

//set routes
route(app, emitter);

server = http.createServer(app);
server.listen(port);
console.log('  info -- app.js || Listening @', ip.address() + ':' + port);

db.connect(mongoUrl, function () {
  console.log('  info -- db.js || connected to db @:', mongoUrl);
});

manager = new Manager(server, emitter);