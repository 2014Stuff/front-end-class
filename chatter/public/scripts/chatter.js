angular.module('chatter', [])
  .controller('chatterCtrl', ['$scope', '$http',
    function ($scope, $http) {
      var wsUrl = 'ws://localhost:4567';
      $scope.ws = null;

      $scope.signedIn = false;
      $scope.auth = null;

      $scope.chat = '';
      $scope.chats = [];

      $scope.user = {};
      $scope.users = [];

      $scope.signout = function () {
        gapi.auth.signOut();
        $scope.auth = null;
      }

      $scope.unAuthorize = function () {
        $http.get('https://accounts.google.com/o/oauth2/revoke?token=' +
          gapi.auth.getToken().access_token)
          .success(function (data) {
            console.log(data);
          }).error(function (err) {
            console.log(err);
          });
      }

      $scope.signin = function (authResult) {
        //authResult from google
        if (authResult['status']['signed_in']) {
          $scope.auth = true;

          //request user from google
          googleUser(function (gUser) {
            //Show the name and img of the user from google
            $scope.$apply(function () {
              $scope.user.displayName = gUser.displayName;
              $scope.user.image = gUser.image;
            });

            //Try to find user in db, else if not found save new user
            console.log('Retrieving user:', gUser.id, 'from db');
            $scope.getUser(gUser.id)
              .success(function (user) {
                if (user) {
                  $scope.user = user;
                  $scope.connect();
                  $scope.setOtherUsers();
                }
              })
              .error(function (err) {
                console.log(err);
                if (err.indexOf('not found') > -1) {
                  //Brand New User
                  $scope.saveUser(gUser).success(function (usr) {
                    console.log(user);
                    if (usr) {
                      $scope.user = usr;
                      $scope.connect();
                      $scope.setOtherUsers();
                    }
                  }).error(function (err) {
                    console.log(err);
                  });
                } else {
                  //we got probs jim
                }
              });

          });
        } else {
          // Update the app to reflect a signed out user
          // Possible error values:
          //   "user_signed_out" - User is signed-out
          //   "access_denied" - User denied access to your app
          //   "immediate_failed" - Could not automatically log in the user
          console.log('Sign-in state: ' + authResult['error']);
        }
      }

      function googleUser(cb) {
        gapi.client.load('plus', 'v1', function () {
          var request = gapi.client.plus.people.get({
            'userId': 'me'
          });
          request.execute(cb);
        });
      }

      $scope.getUser = function (id) {
        return $http.get('/users/' + id);
      }

      $scope.getAllUsers = function (id) {
        return $http.get('/users/');
      }

      $scope.setOtherUsers = function () {
        //Get all users from the db
        $scope.getAllUsers()
          .success(function (users) {
            console.log('getAllUsers result:', users);
            //remove friends and userId from other users
            for (var u in users) {
              if (users[u].id === $scope.user.id) {
                users.splice(u, 1);
              }
              for (var f in $scope.user.friends) {
                if ($scope.user.friends[f] === users[u].id) {
                  users.splice(u, 1);
                }
              }
            }
            $scope.users = users;
          })
          .error(function (err) {
            console.log('getAllUsers err:', err);
          });
      }

      $scope.saveUser = function (user) {
        return $http.post('/users', user);
      }

      $scope.addFriend = function (id, friendId) {
        return $http.put('/users/' + id + '/' + friendId);
      }

      $scope.removeFriend = function (id, friendId) {
        return $http.delete('/users/' + id + '/' + friendId);
      }

      $scope.postChat = function () {
        var chat = {
          type: 'chat',
          owner: $scope.user.id,
          content: {
            text: $scope.chat
          }
        };
        return $http.post('/chats', chat);
      }

      $scope.connect = function () {
        $scope.ws = new WebSocket(wsUrl);

        $scope.ws.onopen = function () {
          console.log('ws connected');
          var reg = {
            type: 'register',
            id: $scope.user.id
          }
          $scope.ws.send(angular.toJson(reg));
        };

        $scope.ws.onmessage = function (event) {
          var msg = JSON.parse(event.data);
          if (msg.type === 'chat') {
            console.log('pushing msg:', msg);
            $scope.getUser(msg.owner)
              .success(function (user) {
                msg.displayName = user.displayName;
                msg.imgUrl = user.image.url;
                msg.timestamp = new Date();
                $scope.chats.push(msg);
              })
              .error(function (err) {
                console.log('onmessage err:', err);
              })
          }
        }
      }

      $scope.chatIt = function () {
        if ($scope.chat.length > 0) {
          $scope.postChat()
            .success(function (data) {
              console.log(data);
              $scope.chat = '';
            })
            .error(function (err) {
              console.log(err);
              $scope.chat = '';
            });
        }
      }

      $scope.follow = function (index) {
        //update view
        $scope.user.friends.push($scope.users[index].id);

        //update ws model
        var msg = {
          type: 'add',
          id: $scope.user.id,
          friendId: $scope.users[index].id
        };
        $scope.ws.send(angular.toJson(msg));

        //save friend to db
        $scope.addFriend($scope.user.id, $scope.users[index].id)
          .success(function (data) {
            console.log('addFriend result:', data);
            //
          })
          .error(function (err) {
            console.log(err);
          });
      }

      $scope.unfollow = function (index) {
        //update model
        $scope.user.friends.splice($scope.user.friends.indexOf($scope.users[index].id), 1);

        //update ws model
        var msg = {
          type: 'remove',
          id: $scope.user.id,
          updates: $scope.user
        };
        $scope.ws.send(angular.toJson(msg));

        //save friend to db
        $scope.removeFriend($scope.user.id, $scope.users[index].id)
          .success(function (data) {
            console.log('addFriend result:', data);
            //
          })
          .error(function (err) {
            console.log(err);
          });
      }
    }
  ]);