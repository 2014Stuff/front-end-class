var db = require('./db');

var chatSchema = new db.Schema({
  timestamp: Date,
  owner: String,
  content: Object,
  language: String
});

var Chat = db.mongoose.model('Chat', chatSchema);

//Create

/**
 *	#createNewChat
 *
 *	cred: a JSON object with name, and password key value pairs
 *	callback:
 *		- err: Error if one occurs
 *		- chat: The newly saved chat object
 */
module.exports.createNewChat = function (blob, callback) {
  var chat = new Chat();
  chat.timestamp = new Date();
  chat.owner = blob.owner;
  chat.content = blob.content;
  chat.language = blob.language || 'en';

  chat.save(callback);
}

//Read
/**
 *	#getAllChats
 *
 *	callback:
 *		- err: Error if one occurs
 *		- chats: An array of Chat objects as saved in the db
 */
module.exports.getAllChats = function (callback) {
  Chat.find(callback);
}

/**
 *  #getOwnerChats
 *
 *  callback:
 *    - err: Error if one occurs
 *    - chats: An array of Chat objects as saved in the db
 */
module.exports.getOwnerChats = function (id, callback) {
  Chat.find({
    owner: id
  }, callback);
}

module.exports.getChat = function (id, callback) {
  Chat.findOne({
    _id: id
  }, callback)
}

module.exports.getLatestChat = function (id, callback) {
  Chat.findOne({
    owner: id
  }, {}, {
    sort: {
      'timestamp': -1
    }
  }, callback);
}

module.exports.getRecentChats = function (id, limit, callback) {
  var limit = limit || 10;
  Chat.find({
    owner: id
  }, {}, {
    sort: {
      'timestamp': -1
    },
    limit: limit
  }, callback);
}

//Update
/**
 *  #updateChat
 *
 *  id: The id of the chat you would like to update
 *  update: object of updates to be made
 *  callback:
 *    - err: Error if one occurs
 *    - chats: An array of Chat objects as saved in the db
 *
 */
module.exports.updateChat = function (id, updates, callback) {
  Chat.findOne({
    _id: id
  }, function (err, chat) {
    if (err) {
      callback(err);
    } else if (!chat) {
      callback(new Error('not found'));
    } else {
      for (var i in updates) {
        chat[i] = updates[i];
      }
      chat.save(callback);
    }
  })
}

//Delete
/**
 *	#deleteChat
 *
 *	id: The id of the chat you would like to delete
 *	updates: A JSON object of the key:values to update, (omitted key values are erased)
 *	callback:
 *		- err: Error if one occurs
 */
module.exports.deleteChat = function (id, callback) {
  Chat.remove({
    _id: id
  }, callback);
}